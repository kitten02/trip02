$(function () {
    //给删除按钮绑定点击事件
    $(".deleteBtn").click(function () {
        var url = $(this).data("url");
        //弹出消息确认框,询问用户是否真的要删除
        $.messager.confirm("温馨提示", "亲,真的要删除吗", function () {
            //发送ajax请求执行删除操作
            $.get(url, function (data) {
                doSuccess();
            });
        });
    });
});

//禁用jQuery数组参数自动加[]
$.ajaxSettings.traditional = true;

$.messager.model = {
    ok: {
        text: "确定"
    },
    cancel: {
        text: "取消"
    }
};

//操作成功执行提示和刷新的方法
function doSuccess(url) {
    $.messager.alert("温馨提示", "操作成功,1.5S后自动刷新");
    setTimeout(function () {
        if (url) {
            location.href = url;
            return;
        }
        location.reload(); //刷新界面
    }, 1500);
}
