﻿<!DOCTYPE html>
<html lang="zh_CN">
<head>
    <#include "../common/header.ftl"/>
</head>
<body>

<div class="container " style="margin-top: 20px">
    <#include "../common/top.ftl"/>

    <div class="row">
        <div class="col-sm-3">
            <#assign menu="travelCommend"/>
            <#include "../common/menu.ftl"/>
        </div>
        <div class="col-sm-9">
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="page-head-line">游记推荐管理</h1>
                </div>
            </div>

            <form class="form-inline" id="searchForm" action="/travelCommend/list.do" method="post">
                <input type="hidden" name="currentPage" id="currentPage" value="1">
            </form>

            <table class="table table-striped table-hover" >
                <tr>
                    <th>序号</th>
                    <th>封面</th>
                    <th>标题</th>
                    <th>副标题</th>
                    <th>推荐时间安排</th>
                    <th>推荐类型</th>
                    <th></th>
                </tr>

                <#list pageInfo.list as entity>
                    <tr>
                        <td>${entity_index + 1}</td>
                        <td><img src="${entity.coverUrl}" width="100px"/></td>
                        <td>${entity.title}</td>
                        <td>${entity.subTitle}</td>
                        <td>${entity.schedule?string("yyyy-MM-dd")}</td>
                        <td>${entity.typeName}</td>
                        <td>
                            <a href="javascript:void(0);" class="inputBtn" data-json='${entity.json}'>修改</a>
                        </td>
                    </tr>
                </#list>
            </table>

            <#include "../common/page.ftl" />
        </div>
    </div>
</div>

<div class="modal fade" id="contentModal" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
              <#--模态框的头部-->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" >查看内容</h4>
            </div>
            <#--模态框的正文-->
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
            </div>
        </div>
    </div>
</div>

<div id="travelCommendModal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">编辑/增加</h4>
            </div>
            <div class="modal-body">
                <form id="commendForm" class="form-horizontal" method="post" action="/travelCommend/saveOrUpdate.do" enctype="multipart/form-data" style="margin: -3px 118px">
                    <!--游记推荐的id-->
                    <input id="traveCommendlId" type="hidden" name="id" />
                    <!--游记的id-->
                    <input id="travelId" type="hidden" name="travelId" />
                    <div class="form-group">
                        <label class="col-sm-4 control-label">标题</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="title" placeholder="标题">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">副标题</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="subTitle" placeholder="副标题">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">封面</label>
                        <div class="col-sm-6">
                            <img  id="coverImg" width="200px"/> <!--用来显示的-->
                            <input type="hidden" name="coverUrl"  > <!--用来提交表单 -->
                            <input type="file" class="form-control"  name="file" ><!--用来上传图片的-->
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">推荐时间</label>
                        <div class="col-sm-6">
                            <input type="text" name="schedule" class="form-control" onclick="WdatePicker()">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">推荐类型</label>
                        <div class="col-sm-6">
                            <select class="form-control" autocomplete="off" name="type" >
                                <option value="1">每月推荐</option>
                                <option value="2">每周推荐</option>
                                <option value="3">攻略推荐</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group" style="text-align: center">
                    <a id="skip" target="_blank" >点击查询游记明细</a>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <a href="javascript:void(0);" class="btn btn-success" id="saveCommendBtn" aria-hidden="true">保存</a>
                <a href="javascript:void(0);" class="btn" data-dismiss="modal" aria-hidden="true">关闭</a>
            </div>
        </div>
    </div>
</div>

<script>
    $(".inputBtn").click(function () {
        var json = $(this).data("json");
        //设置游记推荐对象自己的id
        $("#traveCommendlId").val(json.id);
        //设置游记推荐对象关联的游记id
        $("#travelId").val(json.travelId);
        $("#commendForm input[name='title']").val(json.title);
        $("#commendForm input[name='subTitle']").val(json.subTitle);
        $("#commendForm input[name='type']").val(json.type);
        $("#commendForm input[name='schedule']").val(json.schedule);
        //img图片显示
        $("#coverImg").attr('src',json.coverUrl);
        $("#commendForm input[name='coverUrl']").val(json.coverUrl);

        //处理a链接地址(传入游记的id)
        $("#skip").attr("href","/travel/releaseList.do?travelId="+json.travelId);

        $("#travelCommendModal").modal("show");
    })


    $("#saveCommendBtn").click(function () {
        $("#commendForm").ajaxSubmit(function () {
            doSuccess();
        })
    })


</script>



</body>
</html>