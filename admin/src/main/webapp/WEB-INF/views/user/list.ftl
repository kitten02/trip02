﻿<!DOCTYPE html>
<html lang="zh_CN">
<head>
    <#include "../common/header.ftl"/>
</head>
<body>

<div class="container " style="margin-top: 20px">
    <#include "../common/top.ftl"/>

    <div class="row">
        <div class="col-sm-3">
            <#assign menu="user"/>
            <#include "../common/menu.ftl"/>
        </div>
        <div class="col-sm-9">
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="page-head-line">注册用户列表</h1>
                </div>
            </div>

            <form class="form-inline" id="searchForm" action="/user/list.do" method="post">
                <input type="hidden" name="currentPage" id="currentPage" value="1">
                <div class="form-group">
                    <label>关键字:</label>
                    <input type="text" class="form-control" name="keyword"
                           value="${(qo.keyword)!}" placeholder="请输入昵称/邮箱">
                </div>
                <button type="submit" class="btn btn-primary">
                    <span class="glyphicon glyphicon-search"></span> 查询
                </button>
            </form>

            <table class="table table-striped table-hover" >
                <tr>
                    <th>序号</th>
                    <th>邮箱</th>
                    <th>昵称</th>
                    <th>地区</th>
                    <th>头像</th>
                    <th>性别</th>
                    <th>签名</th>
                </tr>

                <#list pageInfo.list as entity>
                    <tr>
                        <td>${entity_index + 1}</td>
                        <td>${entity.email}</td>
                        <td>${entity.nickName}</td>
                        <td>${entity.place}</td>
                        <td><img src="${entity.headImgUrl}" width="30px"/></td>
                        <td>${entity.genderName}</td>
                        <td>${entity.sign}</td>
                    </tr>
                </#list>
            </table>

            <#include "../common/page.ftl" />
        </div>
    </div>
</div>

</body>
</html>