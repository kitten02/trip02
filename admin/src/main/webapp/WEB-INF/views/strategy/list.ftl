﻿<!DOCTYPE html>
<html lang="zh_CN">
<head>
    <#include "../common/header.ftl"/>
</head>
<body>

<div class="container " style="margin-top: 20px">
    <#include "../common/top.ftl"/>

    <div class="row">
        <div class="col-sm-3">
            <#assign menu="strategy"/>
            <#include "../common/menu.ftl"/>
        </div>
        <div class="col-sm-9">
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="page-head-line">大攻略管理</h1>
                </div>
            </div>

            <form class="form-inline" id="searchForm" action="/strategy/list.do" method="post">
                <input type="hidden" name="currentPage" id="currentPage" value="1">
                <div class="form-group">
                    <label>关键字:</label>
                    <input type="text" class="form-control" name="keyword"
                           value="${(qo.keyword)!}" placeholder="请输入姓名/邮箱">
                </div>
                <button type="submit" class="btn btn-primary">
                    <span class="glyphicon glyphicon-search"></span> 查询
                </button>
                <a href="javascript:;" class="btn btn-success inputBtn">
                    <span class="glyphicon glyphicon-plus"></span> 添加
                </a>
            </form>


            <table class="table table-striped table-hover" >
                <tr>
                    <th>序号</th>
                    <th width="200px">封面</th>
                    <th>攻略标题</th>
                    <th>副标题</th>
                    <th>所属地区</th>
                    <th>状态</th>
                    <th></th>
                </tr>


                <#list pageInfo.list as entity>
                    <tr>
                        <td>${entity_index + 1}</td>
                        <td><img src="${entity.coverUrl}" width="100px"/></td>
                        <td>${entity.title}</td>
                        <td>${entity.subTitle}</td>
                        <td>${entity.place.name}</td>
                        <td>${entity.stateName}</td>
                        <td>
                            <a href="javascript:void(0);" class="inputBtn" data-json='${entity.json}'>修改</a>
                        </td>
                    </tr>
                </#list>
            </table>

            <#include "../common/page.ftl" />
        </div>
    </div>
</div>


<div id="inputModal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">编辑/增加</h4>
            </div>
            <div class="modal-body">
                <form id="editForm" class="form-horizontal" method="post" action="/strategy/saveOrUpdate.do" enctype="multipart/form-data" style="margin: -3px 118px">
                    <input  type="hidden" name="id" />
                    <div class="form-group">
                        <label class="col-sm-4 control-label">标题</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="title" placeholder="标题">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">副标题</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="subTitle" placeholder="副标题">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">封面</label>
                        <div class="col-sm-6">
                            <img  id="coverImg" width="200px"/> <!--用来显示的-->
                            <input type="hidden" name="coverUrl"  > <!--用来提交表单 -->
                            <input type="file" class="form-control"  name="file" ><!--用来上传图片的-->
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">所属地区</label>
                        <div class="col-sm-6">
                            <select class="form-control" autocomplete="off" name="place.id" >
                                <#list regions as r>
                                    <option value="${r.id}">${r.name}</option>
                                </#list>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">状态</label>
                        <div class="col-sm-6">
                            <select class="form-control" autocomplete="off" name="state" >
                                <option value="-1">禁用</option>
                                <option value="0">普通</option>
                                <option value="1">推荐</option>
                            </select>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <a href="javascript:void(0);" class="btn btn-success" id="saveBtn" aria-hidden="true">保存</a>
                <a href="javascript:void(0);" class="btn" data-dismiss="modal" aria-hidden="true">关闭</a>
            </div>
        </div>
    </div>
</div>

<script>
    $(".inputBtn").click(function () {
        var json = $(this).data("json");
        if(json){
            $("#editForm input[name='id']").val(json.id);
            $("#editForm input[name='title']").val(json.title);
            $("#editForm input[name='subTitle']").val(json.subTitle);
            $("#editForm select[name='state']").val(json.state);
            $("#editForm select[name='place.id']").val(json.placeId);
            //img图片显示
            $("#coverImg").attr('src',json.coverUrl);
            $("#editForm input[name='coverUrl']").val(json.coverUrl);
        }

        $("#inputModal").modal("show");
    })


    $("#saveBtn").click(function () {
        $("#editForm").ajaxSubmit(function () {
            doSuccess();
        })
    })


</script>



</body>
</html>