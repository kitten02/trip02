﻿<!DOCTYPE html>
<html lang="zh_CN">
<head>
    <#include "../common/header.ftl"/>
</head>
<body>

<div class="container " style="margin-top: 20px">
    <#include "../common/top.ftl"/>

    <div class="row">
        <div class="col-sm-3">
            <#assign menu="strategyDetail"/>
            <#include "../common/menu.ftl"/>
        </div>
        <div class="col-sm-9">
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="page-head-line">攻略文章管理</h1>
                </div>
            </div>

            <form class="form-inline" id="searchForm" action="/strategyDetail/list.do" method="post">
                <input type="hidden" name="currentPage" id="currentPage" value="1">
                <a href="javascript:;" class="btn btn-success inputBtn">
                    <span class="glyphicon glyphicon-plus"></span> 添加
                </a>
            </form>


            <table class="table table-striped table-hover" >
                <tr>
                    <th>序号</th>
                    <th>标题</th>
                    <th>封面</th>
                    <th>发布时间</th>
                    <th>排序</th>
                    <th>攻略类别</th>
                    <th>状态</th>
                    <th></th>
                </tr>

                <#list pageInfo.list as entity>
                    <tr>
                        <td>${entity_index + 1}</td>
                        <td>${entity.title}</td>
                        <td><img src="${entity.coverUrl}" width="50px"/></td>
                        <td>${(entity.releaseTime?string('yyyy-MM-dd HH:mm:ss'))!}</td>
                        <td>${entity.sequence}</td>
                        <td>${entity.catalog.name}</td>
                        <td>${entity.stateName}</td>
                        <td>
                            <a href="javascript:void(0);" class="inputBtn" data-json='${entity.json}'>修改</a>
                        </td>
                    </tr>
                </#list>
            </table>

            <#include "../common/page.ftl" />
        </div>
    </div>
</div>


<div id="inputModal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">编辑/增加</h4>
            </div>
            <div class="modal-body">
                <form id="editForm" class="form-horizontal" method="post" action="/strategyDetail/saveOrUpdate.do" enctype="multipart/form-data" style="margin: -3px 118px">
                    <input  type="hidden" name="id" />
                    <div class="form-group">
                        <label class="col-sm-4 control-label">文章标题</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="title" placeholder="文章标题">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">封面</label>
                        <div class="col-sm-6">
                            <img  id="coverImg" width="200px"/> <!--用来显示的-->
                            <input type="hidden" name="coverUrl"  > <!--用来提交表单 -->
                            <input type="file" class="form-control"  name="file" ><!--用来上传图片的-->
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">排序</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="sequence" >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-4 control-label">是否发布</label>
                        <div class="col-sm-6">
                            <select class="form-control" autocomplete="off" name="state" >
                                <option value="true">是</option>
                                <option value="false">否</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-4 control-label">所属攻略</label>
                        <div class="col-sm-6">
                           <select class="form-control" autocomplete="off"  id="strategySelect"  >
                                <#list strategies as s>
                                    <option value="${s.id}" >${s.title}</option>
                                </#list>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">攻略分类</label>
                        <div class="col-sm-6">
                           <select class="form-control" autocomplete="off" name="catalog.id" id="catalogSelect"   >
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                          <textarea name="strategyContent.content" id="editor1" rows="10" cols="80">
                         </textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <a href="javascript:void(0);" class="btn btn-success" id="saveBtn" aria-hidden="true">保存</a>
                <a href="javascript:void(0);" class="btn" data-dismiss="modal" aria-hidden="true">关闭</a>
            </div>
        </div>
    </div>
</div>
<!--ckeditor编辑器-->
<script src="/js/ckeditor/ckeditor.js"></script>

<script>
    //初始化ck编辑器
    var editor = CKEDITOR.replace( 'editor1' );


    //攻略下拉框
    $("#strategySelect").change(function () {
        //获取选中的值
        var strategyId = $(this).val();
        //根据id查询攻略分类
        $.get('/strategyCatalog/listByStrategyId.do',{strategyId:strategyId},function (data) {
            var temp = "";
            $.each(data,function (index, ele) {
                temp += "<option value='"+ele.id+"'>"+ele.name+"</option>";
            })
            $("#catalogSelect").html(temp);
        })

    })


    $(".inputBtn").click(function () {
        $("#editForm input").val("");
        $("#editForm select").val("");
        editor.setData("");

        var json = $(this).data("json");
        if(json){
            $("#editForm input[name='id']").val(json.id);
            $("#editForm input[name='title']").val(json.title);
            $("#editForm input[name='sequence']").val(json.sequence);
            $("#editForm select[name='state']").val(json.state+"");
            //img图片显示
            $("#coverImg").attr('src',json.coverUrl);
            $("#editForm input[name='coverUrl']").val(json.coverUrl);
            //回显攻略下拉框
            $("#strategySelect").val(json.strategyId);
            //查询该攻略对应的分类集合
            $.get('/strategyCatalog/listByStrategyId.do',{strategyId:json.strategyId},function (data) {
                var temp = "";
                $.each(data,function (index, ele) {
                    temp += "<option value='"+ele.id+"'>"+ele.name+"</option>";
                })
                $("#catalogSelect").html(temp);
                //回显攻略分类
                $("#catalogSelect").val(json.catalogId);
            })

            //获取文章的内容
            $.get('/strategyDetail/getContentById.do',{detailId:json.id},function (data) {
                editor.setData(data.content);
            })

        }

        $("#inputModal").modal("show");
    })


    $("#saveBtn").click(function () {
        //把ckeditor的内容设置到textarea里面
        var data = editor.getData();
        $("#editForm textarea[name='strategyContent.content']").html(data);

        $("#editForm").ajaxSubmit(function () {
            doSuccess();
        })
    })


</script>



</body>
</html>