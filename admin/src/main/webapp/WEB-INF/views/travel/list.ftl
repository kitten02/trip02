﻿<!DOCTYPE html>
<html lang="zh_CN">
<head>
    <#include "../common/header.ftl"/>
</head>
<body>

<div class="container " style="margin-top: 20px">
    <#include "../common/top.ftl"/>

    <div class="row">
        <div class="col-sm-3">
            <#assign menu="travel"/>
            <#include "../common/menu.ftl"/>
        </div>
        <div class="col-sm-9">
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="page-head-line">待审核游记列表</h1>
                </div>
            </div>

            <form class="form-inline" id="searchForm" action="/travel/list.do" method="post">
                <input type="hidden" name="currentPage" id="currentPage" value="1">
            </form>

            <table class="table table-striped table-hover" >
                <tr>
                    <th>序号</th>
                    <th>标题</th>
                    <th>封面</th>
                    <th>地点</th>
                    <th>作者</th>
                    <th>状态</th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>

                <#list pageInfo.list as entity>
                    <tr>
                        <td>${entity_index + 1}</td>
                        <td>${entity.title}</td>
                        <td><img src="${entity.coverUrl}" width="30px"/></td>
                        <td>${entity.place.name}</td>
                        <td>${entity.author.nickName}</td>
                        <td>${entity.stateName}</td>
                        <td>
                            <a href="javascript:void(0);" class="changeStateBtn" data-state="2" data-tid="${entity.id}">发布</a>
                        </td>
                        <td>
                            <a href="javascript:void(0);" class="changeStateBtn" data-state="-1" data-tid="${entity.id}">拒绝</a>
                        </td>
                        <td>
                            <a href="javascript:void(0);" class="lookBtn" data-tid="${entity.id}" >查看文章</a>
                        </td>

                    </tr>
                </#list>
            </table>

            <#include "../common/page.ftl" />
        </div>
    </div>
</div>

<div class="modal fade" id="contentModal" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
              <#--模态框的头部-->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" >查看内容</h4>
            </div>
            <#--模态框的正文-->
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
            </div>
        </div>
    </div>
</div>

<script>
    //查看文章内容
    $(".lookBtn").click(function () {
        //发送请求获取游记内容
        var id = $(this).data("tid");
        $.get("/travel/getContentById.do",{id:id},function (data) {
            $("#contentModal .modal-body").html(data.content);
        })
        $("#contentModal").modal("show");

    })
    //发布/拒绝
    $(".changeStateBtn").click(function () {
        var id = $(this).data("tid");
        var state = $(this).data("state");

        $.post("/travel/changeState.do",{id:id,state:state},function () {
            doSuccess();
        })
    })



</script>

<style>
    #contentModal img{
        width:100%
    }

    #contentModal .modal-body{
        max-height: 600px;
        overflow-y: scroll;
    }
</style>

</body>
</html>