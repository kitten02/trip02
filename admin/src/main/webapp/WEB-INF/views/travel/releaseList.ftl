﻿<!DOCTYPE html>
<html lang="zh_CN">
<head>
    <#include "../common/header.ftl"/>
</head>
<body>

<div class="container " style="margin-top: 20px">
    <#include "../common/top.ftl"/>

    <div class="row">
        <div class="col-sm-3">
            <#assign menu="travelRelease"/>
            <#include "../common/menu.ftl"/>
        </div>
        <div class="col-sm-9">
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="page-head-line">已发布游记列表</h1>
                </div>
            </div>

            <form class="form-inline" id="searchForm" action="/travel/releaseList.do" method="post">
                <input type="hidden" name="currentPage" id="currentPage" value="1">
            </form>

            <table class="table table-striped table-hover" >
                <tr>
                    <th>序号</th>
                    <th>标题</th>
                    <th>封面</th>
                    <th>地点</th>
                    <th>作者</th>
                    <th>状态</th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>

                <#list pageInfo.list as entity>
                    <tr>
                        <td>${entity_index + 1}</td>
                        <td>${entity.title}</td>
                        <td><img src="${entity.coverUrl}" width="30px"/></td>
                        <td>${entity.place.name}</td>
                        <td>${entity.author.nickName}</td>
                        <td>${entity.stateName}</td>
                        <td>
                            <a href="javascript:void(0);" class="lookBtn" data-tid="${entity.id}" >查看文章</a>
                        </td>
                        <td>
                            <a href="javascript:void(0);" class="changeStateBtn" data-state="-1" data-tid="${entity.id}">取消发布</a>
                        </td>
                        <td>
                            <a href="javascript:void(0);" class="commendBtn"  data-json='${entity.json}' >推荐</a>
                        </td>
                    </tr>
                </#list>
            </table>

            <#include "../common/page.ftl" />
        </div>
    </div>
</div>

<div class="modal fade" id="contentModal" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
              <#--模态框的头部-->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" >查看内容</h4>
            </div>
            <#--模态框的正文-->
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
            </div>
        </div>
    </div>
</div>


<div id="travelCommendModal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">编辑/增加</h4>
            </div>
            <div class="modal-body">
                <form id="commendForm" class="form-horizontal" method="post" action="/travelCommend/saveOrUpdate.do" enctype="multipart/form-data" style="margin: -3px 118px">
                    <!--游记的id-->
                    <input id="travelId" type="hidden" name="travelId" />
                    <div class="form-group">
                        <label class="col-sm-4 control-label">标题</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="title" placeholder="标题">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">副标题</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="subTitle" placeholder="副标题">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">封面</label>
                        <div class="col-sm-6">
                            <img  id="coverImg" width="200px"/> <!--用来显示的-->
                            <input type="hidden" name="coverUrl"  > <!--用来提交表单 -->
                            <input type="file" class="form-control"  name="file" ><!--用来上传图片的-->
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">推荐时间</label>
                        <div class="col-sm-6">
                            <input type="text" name="schedule" class="form-control" onclick="WdatePicker()">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">推荐类型</label>
                        <div class="col-sm-6">
                            <select class="form-control" autocomplete="off" name="type" >
                                <option value="1">每月推荐</option>
                                <option value="2">每周推荐</option>
                                <option value="3">攻略推荐</option>
                            </select>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <a href="javascript:void(0);" class="btn btn-success" id="saveCommendBtn" aria-hidden="true">保存</a>
                <a href="javascript:void(0);" class="btn" data-dismiss="modal" aria-hidden="true">关闭</a>
            </div>
        </div>
    </div>
</div>


<script>
    //查看文章内容
    $(".lookBtn").click(function () {
        //发送请求获取游记内容
        var id = $(this).data("tid");
        $.get("/travel/getContentById.do",{id:id},function (data) {
            $("#contentModal .modal-body").html(data.content);
        })
        $("#contentModal").modal("show");

    })
    //发布/拒绝
    $(".changeStateBtn").click(function () {
        var id = $(this).data("tid");
        var state = $(this).data("state");

        $.post("/travel/changeState.do",{id:id,state:state},function () {
            doSuccess();
        })
    })
    //推荐按钮
    $(".commendBtn").click(function () {
        var json = $(this).data("json");
        //设置游记推荐对象关联的游记id
        $("#travelId").val(json.id);
        $("#commendForm input[name='title']").val(json.title);
        //img图片显示
        $("#coverImg").attr('src',json.coverUrl);
        $("#commendForm input[name='coverUrl']").val(json.coverUrl);

        $("#travelCommendModal").modal("show");
    })

    $("#saveCommendBtn").click(function () {
        $("#commendForm").ajaxSubmit(function () {
            doSuccess();
        })
    })

</script>

<style>
    #contentModal img{
        width:100%
    }

    #contentModal .modal-body{
        max-height: 600px;
        overflow-y: scroll;
    }
</style>

</body>
</html>