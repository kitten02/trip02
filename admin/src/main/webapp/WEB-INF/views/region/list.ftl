﻿<!DOCTYPE html>
<html lang="zh_CN">
<head>
    <#include "../common/header.ftl"/>
    <link rel="stylesheet" href="/js/plugins/treeview/bootstrap-treeview.min.css" type="text/css" />
    <script type="text/javascript" src="/js/plugins/treeview/bootstrap-treeview.min.js"></script>

    <script>
        $(function () {
            $.get("/region/listByParentId.do",{type:"tree"},function (data) {
                //一定要按照treeview的格式来返回
                $('#treeview1').treeview({
                    data: [{text:"全部地区", nodes:data}],
                    showTags: true,
                    lazyLoad:function (node) {
                        //发送ajax请求,带上当前点击的节点的id
                        $.get("/region/listByParentId.do",{parentId:node.id,type:"tree"},function (data) {
                            //调用插件的方法来添加节点到父节点上
                            $('#treeview1').treeview('addNode', [ data, node ]);//第一个参数是子节点,第二个是父节点
                        })

                    },
                    //选中事件
                    onNodeSelected:function (event,node) {
                        //发送ajax请求,带上当前点击的节点的id
                        $.get("/region/listByParentId.do",{parentId:node.id},function (data) {
                            var temp = "";
                            $.each(data,function (index, ele) {
                                var btnText = "推荐";
                                if(ele.state==1){
                                    btnText = "取消推荐";
                                }

                                temp += '<tr><td>'+(index+1)+'</td><td>'+ele.name+'</td>'
                                + "<td><a class='inputBtn' data-json='"+ele.json+"'>修改 </a>"
                                        +"<td><a class='changeStateBtn' data-json='"+ele.json+"'>"+btnText+" </a></td>"
                                        "</td></tr>";
                            })
                            $("tbody").html(temp);
                            
                            $(".inputBtn").click(function () {
                                var json = $(this).data('json');
                                if(json){
                                    $("#editForm input[name='id']").val(json.id);
                                    $("#editForm input[name='name']").val(json.name);
                                    $("#editForm input[name='parent.id']").val(json.parentId);
                                    $("#editForm input[name='parent.name']").val(json.parentName);
                                }else{
                                    //获取tree中选中的数据
                                    var nodes = $('#treeview1').treeview('getSelected');
                                    if(nodes.length>0){
                                        $("#editForm input[name='parent.id']").val(nodes[0].id);
                                        $("#editForm input[name='parent.name']").val(nodes[0].text);
                                    }
                                }
                                $("#regionModal").modal('show');
                            })

                            $(".changeStateBtn").click(function () {
                                var json = $(this).data('json');
                                var state = 1;//推荐
                                if(json.state==1){
                                    state = 0;//普通
                                }
                                $.post('/region/changeState.do',{id:json.id,state:state},function (data) {
                                    doSuccess();
                                })

                            })
                            
                        })
                    }

                });
            })

            //异步提交表单
            $("#editForm").ajaxForm(function (data) {
                doSuccess();
            });

            //给表单按钮绑定点击事件,提交表单
            $(".regionSubmitBtn").click(function () {
                $("#editForm").submit();
            });

        })
    </script>
</head>
<body>

<div class="container " style="margin-top: 20px">
    <#include "../common/top.ftl"/>

    <div class="row">
        <div class="col-sm-3">
            <#assign menu="region"/>
            <#include "../common/menu.ftl"/>
        </div>
        <div class="col-sm-9">
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="page-head-line">旅游地区管理</h1>
                </div>
            </div>
            <a role="button" class="btn btn-success inputBtn">
                <span class="glyphicon glyphicon-plus"></span> 添加
            </a>
            <div class="row">
                <div class="col-sm-4">
                    <div id="treeview1"></div>
                </div>
                <div class="col-sm-8">
                    <table class="regionTb table table-striped table-hover" >
                        <thead>
                        <tr>
                            <th>序号</th>
                            <th>地区名称</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="regionModal" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        <#--模态框的头部-->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" >编辑</h4>
            </div>
        <#--模态框的正文-->
            <div class="modal-body">
                <form class="form-horizontal" action="/region/saveOrUpdate.do" method="post" id="editForm">
                    <input type="hidden" name="id">
                    <div class="form-group" >
                        <label class="col-sm-4 control-label">地区名称：</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="name" placeholder="请输入地区名称">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">上级地区：</label>
                        <div class="col-sm-6">
                            <input type="hidden" class="form-control" name="parent.id" >
                            <input type="text" class="form-control" name="parent.name" readonly>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-primary regionSubmitBtn">保存</button>
            </div>
        </div>
    </div>
</div>



</body>
</html>