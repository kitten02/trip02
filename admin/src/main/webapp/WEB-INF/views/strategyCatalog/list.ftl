﻿<!DOCTYPE html>
<html lang="zh_CN">
<head>
    <#include "../common/header.ftl"/>
</head>
<body>

<div class="container " style="margin-top: 20px">
    <#include "../common/top.ftl"/>

    <div class="row">
        <div class="col-sm-3">
            <#assign menu="strategyCatalog"/>
            <#include "../common/menu.ftl"/>
        </div>
        <div class="col-sm-9">
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="page-head-line">攻略目录管理</h1>
                </div>
            </div>

            <form class="form-inline" id="searchForm" action="/strategyCatalog/list.do" method="post">
                <input type="hidden" name="currentPage" id="currentPage" value="1">
                <div class="form-group">
                    <label>关键字:</label>
                    <select class="form-control" autocomplete="off" name="strategyId" id="strategyId" >
                        <#list strategies as s>
                            <option value="${s.id}" >${s.title}</option>
                        </#list>
                    </select>
                    <script>
                        $("#strategyId").val(${(qo.strategyId)!});
                    </script>
                </div>
                <button type="submit" class="btn btn-primary">
                    <span class="glyphicon glyphicon-search"></span> 查询
                </button>
                <a href="javascript:;" class="btn btn-success inputBtn">
                    <span class="glyphicon glyphicon-plus"></span> 添加
                </a>
            </form>


            <table class="table table-striped table-hover" >
                <tr>
                    <th>序号</th>
                    <th>分类名称</th>
                    <th>所属攻略</th>
                    <th>排序</th>
                    <th>状态</th>
                    <th></th>
                </tr>


                <#list pageInfo.list as entity>
                    <tr>
                        <td>${entity_index + 1}</td>
                        <td>${entity.name}</td>
                        <td>${entity.strategy.title}</td>
                        <td>${entity.sequence}</td>
                        <td>${entity.stateName}</td>
                        <td>
                            <a href="javascript:void(0);" class="inputBtn" data-json='${entity.json}'>修改</a>
                        </td>
                    </tr>
                </#list>
            </table>

            <#include "../common/page.ftl" />
        </div>
    </div>
</div>


<div id="inputModal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">编辑/增加</h4>
            </div>
            <div class="modal-body">
                <form id="editForm" class="form-horizontal" method="post" action="/strategyCatalog/saveOrUpdate.do" enctype="multipart/form-data" style="margin: -3px 118px">
                    <input  type="hidden" name="id" />
                    <div class="form-group">
                        <label class="col-sm-4 control-label">分类名称</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="name" placeholder="名称">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">所属攻略</label>
                        <div class="col-sm-6">
                            <select class="form-control" autocomplete="off" name="strategy.id"  >
                                <#list strategies as s>
                                    <option value="${s.id}" >${s.title}</option>
                                </#list>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">排序</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="sequence" >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-4 control-label">状态</label>
                        <div class="col-sm-6">
                            <select class="form-control" autocomplete="off" name="state" >
                                <option value="true">启用</option>
                                <option value="false">禁用</option>
                            </select>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <a href="javascript:void(0);" class="btn btn-success" id="saveBtn" aria-hidden="true">保存</a>
                <a href="javascript:void(0);" class="btn" data-dismiss="modal" aria-hidden="true">关闭</a>
            </div>
        </div>
    </div>
</div>

<script>
    $(".inputBtn").click(function () {
        var json = $(this).data("json");
        if(json){
            $("#editForm input[name='id']").val(json.id);
            $("#editForm input[name='name']").val(json.name);
            $("#editForm select[name='strategy.id']").val(json.strategyId);
            $("#editForm input[name='sequence']").val(json.sequence);
            $("#editForm select[name='state']").val(json.state+"");
        }else{
            //把查询的下拉框的值设置到模态框中的攻略下拉框
            $("#editForm select[name='strategy.id']").val($("#strategyId").val());
        }

        $("#inputModal").modal("show");
    })


    $("#saveBtn").click(function () {
        $("#editForm").ajaxSubmit(function () {
            doSuccess();
        })
    })


</script>



</body>
</html>