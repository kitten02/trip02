package cn.wolfcode.trip.admin.controller;

import cn.wolfcode.trip.base.util.UploadUtil;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.Map;

/**
 * 图片资源控制器
 */
@RestController
@RequestMapping("images")
public class ImageController {

    @PostMapping
    public Map upload(MultipartFile upload){

        HashMap<Object, Object> map = new HashMap<>();
        try {
           // String url = UploadUtil.upload(upload, UploadUtil.PATH + "/upload");
            //上传图片到七牛云
            String url = UploadUtil.uploadQiu(upload);
            map.put("uploaded",1);
            map.put("url",UploadUtil.NIU_PATH+url);//http://phywhaqv1.bkt.clouddn.com/FlSg3E5g6zxGvuplGdkHeIKhQkxt
        } catch (Exception e) {
            e.printStackTrace();
            map.put("uploaded",0);
            HashMap<Object, Object> temp = new HashMap<>();
            temp.put("message","上传失败!");
            map.put("error",temp);
        }
        return map;
    }

}
