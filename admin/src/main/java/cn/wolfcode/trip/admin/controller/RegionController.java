package cn.wolfcode.trip.admin.controller;

import cn.wolfcode.trip.base.domain.Region;
import cn.wolfcode.trip.base.query.RegionQuery;
import cn.wolfcode.trip.base.service.IRegionService;
import cn.wolfcode.trip.base.util.JSONResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("region")
public class RegionController {

    @Autowired
    private IRegionService regionService;

    @RequestMapping("list")
    public String list(){
        return "region/list";
    }

    @RequestMapping("listByParentId")
    @ResponseBody
    public List listByParentId(RegionQuery qo,String type){
        //查询地区的集合
        List<Region> list =  regionService.listByParentId(qo);
        //判断当前如果是获取treeview的数据就进行转换
        if("tree".equals(type)){
            //用于存储map的集合
            ArrayList<Map> maps = new ArrayList<>();
            for (Region region : list) {
                //把每个地区都转换成map集合
                maps.add(region.toTreeMap());
            }
            return maps;
        }
        return list;
    }

    @RequestMapping("saveOrUpdate")
    @ResponseBody
    public JSONResult saveOrUpdate(Region region){
        regionService.saveOrUpdate(region);
        return new JSONResult();
    }

    @RequestMapping("changeState")
    @ResponseBody
    public JSONResult changeState(Long id,Integer state){
        regionService.changeState(id,state);
        return new JSONResult();
    }


}
