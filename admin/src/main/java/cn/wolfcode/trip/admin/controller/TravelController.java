package cn.wolfcode.trip.admin.controller;

import cn.wolfcode.trip.base.domain.Travel;
import cn.wolfcode.trip.base.domain.TravelContent;
import cn.wolfcode.trip.base.query.TravelQuery;
import cn.wolfcode.trip.base.service.ITravelService;
import cn.wolfcode.trip.base.util.JSONResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("travel")
public class TravelController {

    @Autowired
    private ITravelService travelService;

    @RequestMapping("list")
    public String list(@ModelAttribute("qo") TravelQuery qo, Model model){
        //待发布状态
        qo.setState(Travel.STATE_WAIT);
        //公开
        qo.setIsPublic(true);
        //设置排序
        qo.setOrderby("t.lastUpdateTime asc");
        model.addAttribute("pageInfo",travelService.queryForList(qo));
        return "travel/list";
    }

    @RequestMapping("releaseList")
    public String releaseList(@ModelAttribute("qo") TravelQuery qo, Model model){
        //已发布状态
        qo.setState(Travel.STATE_RELEASE);
        //设置排序
        qo.setOrderby("t.releaseTime desc");
        model.addAttribute("pageInfo",travelService.queryForList(qo));
        return "travel/releaseList";
    }


    @RequestMapping("getContentById")
    @ResponseBody
    public TravelContent getContentById(Long id){
       return travelService.getContentById(id);
    }

    @RequestMapping("changeState")
    @ResponseBody
    public JSONResult changeState(Travel travel){
       travelService.changeState(travel);
       return new JSONResult();

    }

}
