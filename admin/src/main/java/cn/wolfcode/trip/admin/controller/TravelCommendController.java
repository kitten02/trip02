package cn.wolfcode.trip.admin.controller;

import cn.wolfcode.trip.base.domain.TravelCommend;
import cn.wolfcode.trip.base.query.TravelQuery;
import cn.wolfcode.trip.base.service.ITravelCommendService;
import cn.wolfcode.trip.base.util.JSONResult;
import cn.wolfcode.trip.base.util.UploadUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

@Controller
@RequestMapping("travelCommend")
public class TravelCommendController {

    @Autowired
    private ITravelCommendService travelCommendService;

    @RequestMapping("list")
    public String list(@ModelAttribute("qo") TravelQuery qo, Model model){
        model.addAttribute("pageInfo",travelCommendService.queryForList(qo));
        return "travelCommend/list";
    }

    @RequestMapping("saveOrUpdate")
    @ResponseBody
    public JSONResult saveOrUpdate(TravelCommend travelCommend, MultipartFile file){
        //判断是否上传图片
        if(file!=null&&file.getSize()>0){
            //上传图片
            String url = UploadUtil.upload(file, UploadUtil.PATH + "/upload");
            //设置图片地址到游记推荐对象中
            travelCommend.setCoverUrl(url);
        }
        travelCommendService.saveOrUpdate(travelCommend);
        return new JSONResult();
    }

}
