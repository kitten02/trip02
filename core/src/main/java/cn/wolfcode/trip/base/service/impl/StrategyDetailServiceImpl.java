package cn.wolfcode.trip.base.service.impl;

import cn.wolfcode.trip.base.domain.StrategyContent;
import cn.wolfcode.trip.base.domain.StrategyDetail;
import cn.wolfcode.trip.base.mapper.StrategyContentMapper;
import cn.wolfcode.trip.base.mapper.StrategyDetailMapper;
import cn.wolfcode.trip.base.query.QueryObject;
import cn.wolfcode.trip.base.service.IStrategyDetailService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by wolfcode-lanxw
 */
@Service
public class StrategyDetailServiceImpl implements IStrategyDetailService {
    @Autowired
    private StrategyDetailMapper strategyDetailMapper;
    @Autowired
    private StrategyContentMapper contentMapper;

    @Override
    public void saveOrUpdate(StrategyDetail detail) {
        StrategyContent strategyContent = detail.getStrategyContent();

        //设置发布时间
        if(detail.getState()){
            detail.setReleaseTime(new Date());
        }

        //判断是否有序号
        if (detail.getSequence()==null){
            //查询数据库中最大的序号
            int maxSequence = strategyDetailMapper.selectMaxSequence(detail.getCatalog().getId());
            detail.setSequence(maxSequence+1);
        }

        if(detail.getId()==null){
            detail.setCreateTime(new Date());
            strategyDetailMapper.insert(detail);
            //主键关联
            strategyContent.setId(detail.getId());
            contentMapper.insert(strategyContent);

        }else{
            strategyDetailMapper.updateByPrimaryKey(detail);
            //主键关联
            strategyContent.setId(detail.getId());
            contentMapper.updateByPrimaryKey(strategyContent);
        }
    }

    @Override
    public PageInfo query(QueryObject qo) {
        PageHelper.startPage(qo.getCurrentPage(),qo.getPageSize());
        List<StrategyDetail> list = strategyDetailMapper.selectForList(qo);
        return new PageInfo(list);
    }

    @Override
    public StrategyContent getContentById(Long detailId) {
        return contentMapper.selectByPrimaryKey(detailId);
    }

    @Override
    public StrategyDetail getById(Long id) {
        return strategyDetailMapper.selectByPrimaryKey(id);
    }
}


