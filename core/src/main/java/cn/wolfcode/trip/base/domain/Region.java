package cn.wolfcode.trip.base.domain;

import cn.wolfcode.trip.base.util.JSONUtil;
import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;

/**
 * 旅游地区
 */
@Setter
@Getter
public class Region extends BaseDomain{
    //普通
    public static final int STATE_NORMAL = 0;
    //推荐
    public static final int STATE_COMMEND = 1;
    //禁用
    public static final int STATE_DISABLE = -1;

    //名称
    private String name;
    //上级地区
    private Region parent;
    //状态
    private Integer state = STATE_NORMAL;

    public Map toTreeMap(){
        HashMap map = new HashMap<>();
        map.put("id",id);
        map.put("text",name);
        map.put("lazyLoad",true);
        if(state==STATE_COMMEND){
            map.put("tags",new String[]{"推荐"});
        }
        return map;
    }

    public String getJson(){
        HashMap map = new HashMap<>();
        map.put("id",id);
        map.put("name",name);
        map.put("state",state);
        if(parent!=null) {
            map.put("parentId", parent.getId());
            map.put("parentName", parent.getName());
        }
        return JSONUtil.toJSONString(map);
    }

}