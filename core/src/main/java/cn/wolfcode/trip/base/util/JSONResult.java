package cn.wolfcode.trip.base.util;

import lombok.Getter;

@Getter
public class JSONResult {
    private boolean success = true;
    private String msg; //错误信息
    private Object result;

    //标记错误信息
    public JSONResult mark(String msg) {
        this.msg = msg;
        success = false;
        return this;
    }

    public JSONResult setResult(Object result) {
        this.result = result;
        return this;
    }
}
