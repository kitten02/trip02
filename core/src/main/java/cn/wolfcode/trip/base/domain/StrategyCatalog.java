package cn.wolfcode.trip.base.domain;


import cn.wolfcode.trip.base.util.JSONUtil;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 攻略分类
 */
@Setter
@Getter
@JsonIgnoreProperties("handler")//转成json的时候忽略序列化该属性
public class StrategyCatalog extends BaseDomain{
    //分类名称
    private String name;
    //所属攻略
    private Strategy strategy;
    //序号
    private Integer sequence;
    //状态
    private Boolean state;
    //文章集合
    private List<StrategyDetail> details = new ArrayList();

    public String getStateName(){
        String temp = "禁用";
        if(state){
            temp = "启用";
        }
        return temp;
    }

    public String getJson(){
        HashMap map = new HashMap<>();
        map.put("id",id);
        map.put("name",name);
        if(strategy!=null) {
            map.put("strategyId", strategy.getId());
            map.put("strategyTitle", strategy.getTitle());
        }
        map.put("sequence",sequence);
        map.put("state",state);
        return JSONUtil.toJSONString(map);
    }
}