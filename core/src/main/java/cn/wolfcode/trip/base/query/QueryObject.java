package cn.wolfcode.trip.base.query;

import lombok.Getter;
import lombok.Setter;
import org.springframework.util.StringUtils;

@Setter@Getter
public class QueryObject {
    private int currentPage = 1;
    private int pageSize = 3;

    private String keyword;

    private String orderby;

    public String getKeyword() {
        return StringUtils.hasLength(keyword) ? keyword : null;
    }
}
