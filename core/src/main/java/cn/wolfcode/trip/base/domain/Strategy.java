package cn.wolfcode.trip.base.domain;

import cn.wolfcode.trip.base.util.JSONUtil;
import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;

/**
 * 大攻略
 */
@Setter
@Getter
public class Strategy extends BaseDomain{
    //普通
    public static final int STATE_NORMAL = 0;
    //推荐
    public static final int STATE_COMMEND = 1;
    //禁用
    public static final int STATE_DISABLE = -1;

    //地区
    private Region place;
    //标题
    private String title;
    //副标题
    private String subTitle;
    //封面
    private String coverUrl;
    //状态
    private Integer state;

    public String getStateName(){
        String temp = "禁用";
        switch (state) {
            case STATE_NORMAL:
                temp = "普通";
                break;
            case STATE_COMMEND:
                temp = "推荐";
                break;
        }
        return temp;
    }

    public String getJson(){
        HashMap map = new HashMap<>();
        map.put("id",id);
        map.put("title",title);
        map.put("subTitle",subTitle);
        map.put("coverUrl",coverUrl);
        if(place!=null) {
            map.put("placeId", place.getId());
        }
        map.put("state",state);
        return JSONUtil.toJSONString(map);
    }
}