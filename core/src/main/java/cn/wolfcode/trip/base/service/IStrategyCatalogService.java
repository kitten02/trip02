package cn.wolfcode.trip.base.service;

import cn.wolfcode.trip.base.domain.StrategyCatalog;
import cn.wolfcode.trip.base.query.QueryObject;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * Created by wolfcode-lanxw
 */
public interface IStrategyCatalogService {
    /**
     * 新增和编辑
     */
    void saveOrUpdate(StrategyCatalog catalog);

    /**
     * 分页
     * @param qo
     * @return
     */
    PageInfo query(QueryObject qo);

    /**
     * 根据攻略id查询分类集合
     * @param strategyId
     * @return
     */
    List<StrategyCatalog> listByStrategyId(Long strategyId);


}
