package cn.wolfcode.trip.base.service.impl;

import cn.wolfcode.trip.base.domain.Strategy;
import cn.wolfcode.trip.base.mapper.StrategyMapper;
import cn.wolfcode.trip.base.query.QueryObject;
import cn.wolfcode.trip.base.service.IStrategyService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by wolfcode-lanxw
 */
@Service
public class StrategyServiceImpl implements IStrategyService {
    @Autowired
    private StrategyMapper strategyMapper;


    @Override
    public void saveOrUpdate(Strategy strategy) {
        if(strategy.getId()==null){
            strategyMapper.insert(strategy);
        }else{
            strategyMapper.updateByPrimaryKey(strategy);
        }
    }

    @Override
    public PageInfo query(QueryObject qo) {
        PageHelper.startPage(qo.getCurrentPage(),qo.getPageSize());
        List<Strategy> list = strategyMapper.selectForList(qo);
        return new PageInfo(list);
    }

    @Override
    public List<Strategy> listAll() {
        return strategyMapper.selectAll();
    }

    @Override
    public Strategy getById(Long id) {
        return strategyMapper.selectByPrimaryKey(id);
    }
}


