package cn.wolfcode.trip.base.service;

import cn.wolfcode.trip.base.domain.Region;
import cn.wolfcode.trip.base.query.QueryObject;
import cn.wolfcode.trip.base.query.RegionQuery;
import com.github.pagehelper.PageInfo;

import java.util.List;

public interface IRegionService {
    /**
     * 新增和编辑
     * @param region
     */
    void saveOrUpdate(Region region);

    /**
     * 分页
     * @param qo
     * @return
     */
    PageInfo query(QueryObject qo);

    /**
     * 根据上级地区id查询
     * @param qo
     * @return
     */
    List listByParentId(RegionQuery qo);

    /**
     * 修改状态
     * @param id
     * @param state
     */
    void changeState(Long id, Integer state);

    /**
     * 查询全部地区
     * @return
     */
    List<Region> listAll(Integer state);

}
