package cn.wolfcode.trip.base.domain;

import cn.wolfcode.trip.base.util.JSONUtil;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.HashMap;

/**
 * 攻略文章
 */
@Setter
@Getter
public class StrategyDetail extends BaseDomain{

    //标题
    private String title;
    //创建时间
    private Date createTime;
    //发布时间
    private Date releaseTime;
    //序号
    private Integer sequence;
    //所属的攻略分类
    private StrategyCatalog catalog;
    //封面
    private String coverUrl;
    //发布状态(草稿/发布)
    private Boolean state = false;

    //文章内容
    private StrategyContent strategyContent;

    public String getStateName(){
        String temp = "草稿";
        if(state){
            temp = "发布";
        }
        return temp;
    }

    public String getJson(){
        HashMap map = new HashMap<>();
        map.put("id",id);
        map.put("title",title);
        map.put("sequence",sequence);
        map.put("coverUrl",coverUrl);
        if(catalog!=null){
          map.put("catalogId",catalog.getId());
          map.put("strategyId",catalog.getStrategy().getId());
        }
        map.put("state",state);
        return JSONUtil.toJSONString(map);
    }
}