package cn.wolfcode.trip.base.query;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by wolfcode-lanxw
 */
@Setter
@Getter
public class StrategyQuery extends QueryObject {
    private Integer state;//状态
    private Long regionId;//地区id
}
