package cn.wolfcode.trip.base.query;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class TravelCommendQuery extends QueryObject {
    private Integer type;//推荐类型
}
