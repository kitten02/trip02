package cn.wolfcode.trip.base.service.impl;

import cn.wolfcode.trip.base.domain.StrategyCatalog;
import cn.wolfcode.trip.base.mapper.StrategyCatalogMapper;
import cn.wolfcode.trip.base.query.QueryObject;
import cn.wolfcode.trip.base.service.IStrategyCatalogService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by wolfcode-lanxw
 */
@Service
public class StrategyCatalogServiceImpl implements IStrategyCatalogService {
    @Autowired
    private StrategyCatalogMapper strategyCatalogMapper;

    @Override
    public void saveOrUpdate(StrategyCatalog catalog) {
        //判断是否有序号
        if (catalog.getSequence()==null){
            //查询数据库中最大的序号
            int maxSequence = strategyCatalogMapper.selectMaxSequence(catalog.getStrategy().getId());
            catalog.setSequence(maxSequence+1);
        }

        if(catalog.getId()==null){
            strategyCatalogMapper.insert(catalog);
        }else{
            strategyCatalogMapper.updateByPrimaryKey(catalog);
        }
    }

    @Override
    public PageInfo query(QueryObject qo) {
        PageHelper.startPage(qo.getCurrentPage(),qo.getPageSize());
        List<StrategyCatalog> list = strategyCatalogMapper.selectForList(qo);
        return new PageInfo(list);
    }

    @Override
    public List<StrategyCatalog> listByStrategyId(Long strategyId) {
        return strategyCatalogMapper.selectByStrategyId(strategyId);
    }
}


