package cn.wolfcode.trip.base.service.impl;

import cn.wolfcode.trip.base.domain.StrategyComment;
import cn.wolfcode.trip.base.domain.Tag;
import cn.wolfcode.trip.base.mapper.StrategyCommentMapper;
import cn.wolfcode.trip.base.mapper.TagMapper;
import cn.wolfcode.trip.base.query.QueryObject;
import cn.wolfcode.trip.base.service.IStrategyCommentService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by wolfcode-lanxw
 */
@Service
public class StrategyCommentServiceImpl implements IStrategyCommentService {
    @Autowired
    private StrategyCommentMapper strategyCommentMapper;
    
    @Autowired
    private TagMapper tagMapper;


    @Override
    public void saveOrUpdate(StrategyComment strategyComment,String[] tags) {
        if(strategyComment.getId()==null){
            //评论时间
            strategyComment.setCreateTime(new Date());
            strategyCommentMapper.insert(strategyComment);

            //把标签保存到数据库中
            for (String temp : tags) {
                Tag tag = new Tag();
                tag.setName(temp);
                tagMapper.insert(tag);
                //把评论id和标签id保存到中间表
                strategyCommentMapper.insertRelation(strategyComment.getId(),tag.getId());
            }
        }else{
            strategyCommentMapper.updateByPrimaryKey(strategyComment);
        }
    }

    @Override
    public PageInfo query(QueryObject qo) {
        PageHelper.startPage(qo.getCurrentPage(),qo.getPageSize(),qo.getOrderby());
        List<StrategyComment> list = strategyCommentMapper.selectForList(qo);
        return new PageInfo(list);
    }

}


