package cn.wolfcode.trip.base.service;

import cn.wolfcode.trip.base.domain.TravelCommend;
import cn.wolfcode.trip.base.query.QueryObject;
import cn.wolfcode.trip.base.query.TravelCommendQuery;
import com.github.pagehelper.PageInfo;

public interface ITravelCommendService {
    /**
     * 分页
     * @param qo
     * @return
     */
    PageInfo queryForList(QueryObject qo);

    /**
     * 新增和编辑
     * @param travelCommend
     */
    void saveOrUpdate(TravelCommend travelCommend);

    /**
     * 为移动端定制的分页sql
     * @param qo
     * @return
     */
    PageInfo queryForAppList(TravelCommendQuery qo);
}
