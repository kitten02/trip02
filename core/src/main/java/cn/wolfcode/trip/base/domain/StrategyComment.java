package cn.wolfcode.trip.base.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * 攻略评论
 */
@Setter
@Getter
public class StrategyComment extends BaseDomain{

    //普通
    public static final int STATE_NORMAL = 0;
    //推荐
    public static final int STATE_COMMEND = 1;
    //禁用
    public static final int STATE_DISABLE = -1;

    //评论者
    private User user;
    //评论时间
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private Date createTime;
    //评论内容
    private String content;
    //评论图片路径
    private String imgUrls;
    //星星数
    private Integer starNum;
    //攻略
    private Strategy strategy;
    //状态
    private Integer state = STATE_NORMAL;
    //推荐时间
    private Date commendTime;

    public String[] getImgArr(){
        if (imgUrls!=null){
            return imgUrls.split(";");
        }
        return null;
    }

}