package cn.wolfcode.trip.base.service.impl;

import cn.wolfcode.trip.base.domain.User;
import cn.wolfcode.trip.base.mapper.UserMapper;
import cn.wolfcode.trip.base.query.QueryObject;
import cn.wolfcode.trip.base.service.IUserService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements IUserService {
    @Autowired
    private UserMapper userMapper;

    @Override
    public void saveOrUpdate(User user) {
        if(user.getId()==null){
            //检查邮箱是否已经被注册
            User temp = userMapper.selectByEmailAndPassword(user.getEmail(), null);
            if(temp!=null){
                //邮箱已存在
                throw new RuntimeException("邮箱已被注册!");
            }
            //设置默认的头像和封面
            user.setCoverImgUrl("/img/user/bg.jpeg");
            user.setHeadImgUrl("/img/user/head.jpg");
            userMapper.insert(user);
        }else{
            userMapper.updateByPrimaryKey(user);
        }
    }

    @Override
    public User login(String email, String password) {
        User temp = userMapper.selectByEmailAndPassword(email, password);
        if(temp==null){
            throw new RuntimeException("账号和密码不匹配!");
        }
        return temp;
    }

    @Override
    public PageInfo query(QueryObject qo) {
        PageHelper.startPage(qo.getCurrentPage(),qo.getPageSize());
        List<User> list = userMapper.selectForList(qo);
        return new PageInfo(list);
    }
}
