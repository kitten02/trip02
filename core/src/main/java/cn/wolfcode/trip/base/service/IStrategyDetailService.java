package cn.wolfcode.trip.base.service;

import cn.wolfcode.trip.base.domain.StrategyContent;
import cn.wolfcode.trip.base.domain.StrategyDetail;
import cn.wolfcode.trip.base.query.QueryObject;
import com.github.pagehelper.PageInfo;

/**
 * Created by wolfcode-lanxw
 */
public interface IStrategyDetailService {
    /**
     * 新增和编辑
     */
    void saveOrUpdate(StrategyDetail detail);

    /**
     * 分页
     * @param qo
     * @return
     */
    PageInfo query(QueryObject qo);


    /**
     * 根据id获取文章内容
     * @param detailId
     * @return
     */
    StrategyContent getContentById(Long detailId);

    /**
     * 根据id获取文章对象
     * @param id
     * @return
     */
    StrategyDetail getById(Long id);
}
