package cn.wolfcode.trip.base.service.impl;

import cn.wolfcode.trip.base.domain.Region;
import cn.wolfcode.trip.base.mapper.RegionMapper;
import cn.wolfcode.trip.base.query.QueryObject;
import cn.wolfcode.trip.base.query.RegionQuery;
import cn.wolfcode.trip.base.service.IRegionService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RegionServiceImpl implements IRegionService {
    @Autowired
    private RegionMapper regionMapper;

    @Override
    public void saveOrUpdate(Region region) {
        if(region.getId()==null){
            regionMapper.insert(region);
        }else{
            regionMapper.updateByPrimaryKey(region);
        }
    }


    @Override
    public PageInfo query(QueryObject qo) {
        PageHelper.startPage(qo.getCurrentPage(),qo.getPageSize());
        List<Region> list = regionMapper.selectForList(qo);
        return new PageInfo(list);
    }

    @Override
    public List listByParentId(RegionQuery qo) {
        return regionMapper.selectByParentId(qo);
    }

    @Override
    public void changeState(Long id, Integer state) {
        regionMapper.changeState(id,state);
    }

    @Override
    public List<Region> listAll(Integer state) {
        return regionMapper.selectAll(state);
    }
}
