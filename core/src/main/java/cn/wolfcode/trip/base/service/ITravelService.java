package cn.wolfcode.trip.base.service;

import cn.wolfcode.trip.base.domain.Travel;
import cn.wolfcode.trip.base.domain.TravelContent;
import cn.wolfcode.trip.base.query.TravelQuery;
import com.github.pagehelper.PageInfo;

public interface ITravelService {
    /**
     * 分页
     * @param qo
     * @return
     */
    PageInfo queryForList(TravelQuery qo);

    /**
     * 新增和编辑
     * @param travel
     */
    void saveOrUpdate(Travel travel);

    /**
     * 跟据游记id查询游记对象
     * @param id
     * @return
     */
    Travel getById(Long id);

    /**
     * 根据游记id获取游记内容
     * @param id
     * @return
     */
    TravelContent getContentById(Long id);

    /**
     * 修改游记状态
     * @param travel
     */
    void changeState(Travel travel);
}
