package cn.wolfcode.trip.base.service;

import cn.wolfcode.trip.base.domain.StrategyComment;
import cn.wolfcode.trip.base.query.QueryObject;
import com.github.pagehelper.PageInfo;

/**
 * Created by wolfcode-lanxw
 */
public interface IStrategyCommentService {
    /**
     * 新增和编辑
     */
    void saveOrUpdate(StrategyComment comment,String[] tags);

    /**
     * 分页
     * @param qo
     * @return
     */
    PageInfo query(QueryObject qo);

}
