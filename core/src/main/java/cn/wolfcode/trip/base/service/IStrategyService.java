package cn.wolfcode.trip.base.service;

import cn.wolfcode.trip.base.domain.Strategy;
import cn.wolfcode.trip.base.query.QueryObject;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * Created by wolfcode-lanxw
 */
public interface IStrategyService {
    /**
     * 新增和编辑
     */
    void saveOrUpdate(Strategy strategy);

    /**
     * 分页
     * @param qo
     * @return
     */
    PageInfo query(QueryObject qo);

    /**
     * 查询全部攻略
     * @return
     */
    List<Strategy> listAll();

    /**
     * 根据id查询大攻略
     * @param id
     * @return
     */
    Strategy getById(Long id);
}
