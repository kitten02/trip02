package cn.wolfcode.trip.base.query;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by wolfcode-lanxw
 */
@Setter
@Getter
public class StrategyCatalogQuery extends QueryObject {
    private Long strategyId;//大攻略id
}
