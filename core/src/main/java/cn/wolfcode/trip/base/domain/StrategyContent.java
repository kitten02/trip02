package cn.wolfcode.trip.base.domain;

import lombok.Getter;
import lombok.Setter;

/**
 * 攻略文章内容
 */
@Setter
@Getter
public class StrategyContent extends BaseDomain{
    //内容
    private String content;

}