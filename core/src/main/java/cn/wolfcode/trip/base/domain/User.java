package cn.wolfcode.trip.base.domain;

import lombok.Getter;
import lombok.Setter;

/**
 * 平台注册用户
 */
@Setter
@Getter
public class User extends BaseDomain{
    //男
    public static final int GENDER_MAN = 1;
    //女
    public static final int GENDER_WOMEN = 0;
    //保密
    public static final int GENDER_SECRET = -1;

    //邮箱
    private String email;
    //昵称
    private String nickName;
    //密码
    private String password;
    //地区
    private String place;
    //头像url
    private String headImgUrl;
    //性别
    private Integer gender = GENDER_SECRET;
    //封面url
    private String coverImgUrl;
    //签名
    private String sign;

    public String getGenderName(){
        String temp = "保密";
        if (this.gender == GENDER_MAN){
            temp = "男";
        }else if (this.gender == GENDER_WOMEN){
            temp = "女";
        }
        return temp;
    }
}