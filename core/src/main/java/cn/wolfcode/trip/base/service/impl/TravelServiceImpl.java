package cn.wolfcode.trip.base.service.impl;

import cn.wolfcode.trip.base.domain.Travel;
import cn.wolfcode.trip.base.domain.TravelContent;
import cn.wolfcode.trip.base.mapper.TravelContentMapper;
import cn.wolfcode.trip.base.mapper.TravelMapper;
import cn.wolfcode.trip.base.query.TravelQuery;
import cn.wolfcode.trip.base.service.ITravelService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class TravelServiceImpl implements ITravelService {

    @Autowired
    private TravelMapper travelMapper;
    @Autowired
    private TravelContentMapper contentMapper;


    @Override
    public PageInfo queryForList(TravelQuery qo) {
        PageHelper.startPage(qo.getCurrentPage(),qo.getPageSize(),qo.getOrderby());
        List<Travel> list = travelMapper.selectForList(qo);
        return new PageInfo(list);
    }

    @Override
    public void saveOrUpdate(Travel travel) {
        Date date = new Date();
        //获取游记内容对象
        TravelContent travelContent = travel.getTravelContent();
        if(travel.getId()!=null){
            //设置最后更新时间
            travel.setLastUpdateTime(date);
            travelMapper.updateByPrimaryKey(travel);
            //更新游记内容
            //设置id关联
            travelContent.setId(travel.getId());
            contentMapper.updateByPrimaryKey(travelContent);

        }else{
            //设置创建时间
            travel.setCreateTime(date);
            //设置最后更新时间
            travel.setLastUpdateTime(date);
            travelMapper.insert(travel);
            //保存游记内容
            //设置id关联
            travelContent.setId(travel.getId());
            contentMapper.insert(travelContent);
        }
    }

    @Override
    public Travel getById(Long id) {
        return travelMapper.selectByPrimaryKey(id);
    }

    @Override
    public TravelContent getContentById(Long id) {
        return contentMapper.selectByPrimaryKey(id);
    }

    @Override
    public void changeState(Travel travel) {
        //判断是否是发布状态,如果是就设置发布时间
        if(travel.getState()==Travel.STATE_RELEASE){
            travel.setReleaseTime(new Date());
        }
        travelMapper.changeState(travel);
    }
}
