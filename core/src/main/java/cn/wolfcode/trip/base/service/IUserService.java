package cn.wolfcode.trip.base.service;

import cn.wolfcode.trip.base.domain.User;
import cn.wolfcode.trip.base.query.QueryObject;
import cn.wolfcode.trip.base.query.TravelQuery;
import com.github.pagehelper.PageInfo;

public interface IUserService {
    /**
     * 新增和编辑
     * @param user
     */
    void saveOrUpdate(User user);

    /**
     * 登录
     * @param email
     * @param password
     */
    User login(String email, String password);

    /**
     * 分页
     * @param qo
     * @return
     */
    PageInfo query(QueryObject qo);


}
