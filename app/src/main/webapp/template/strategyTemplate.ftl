<div class="container">
    <h6>当季推荐</h6>
    <div class="row hot">
        <#list commends.list as s>
            <div class="col ">
                <a href="../strategyCatalogs.html?id=${s.id}">
                    <img src="${s.coverUrl}">
                    <p>0次浏览</p>
                </a>
            </div>
        </#list>

    </div>
</div>
<hr>
<div class="container">
    <h6>全部攻略</h6>
    <div class="row classify ">
        <#list all.list as s>
            <div class="col-6 mb">
                <a href="../strategyCatalogs.html?id=${s.id}">
                    <img class="float-left " src="${s.coverUrl}">
                    <div class="classify-text">
                        <span >${s.title}</span>
                        <p>0人收藏</p>
                    </div>
                </a>
            </div>
        </#list>

    </div>
</div>