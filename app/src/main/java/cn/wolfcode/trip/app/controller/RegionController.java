package cn.wolfcode.trip.app.controller;

import cn.wolfcode.trip.base.domain.Region;
import cn.wolfcode.trip.base.domain.Strategy;
import cn.wolfcode.trip.base.query.StrategyQuery;
import cn.wolfcode.trip.base.service.IRegionService;
import cn.wolfcode.trip.base.service.IStrategyService;
import com.github.pagehelper.PageInfo;
import freemarker.template.Configuration;
import freemarker.template.Template;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.util.HashMap;
import java.util.List;

/**
 * 地区资源控制器
 */
@RestController
@RequestMapping("regions")
public class RegionController {

    @Autowired
    private IRegionService regionService;

    @Autowired
    private IStrategyService strategyService;

    @Autowired
    private ServletContext servletContext;

    @GetMapping
    public List<Region> listAll(Integer state){
       return regionService.listAll(state);
    }

    //前台传accept要求是接收json ,也代表后台生产的数据类型content-type是json类型
    @GetMapping(value="{regionId}/strategies",produces = "application/json;charset=utf-8")
    public PageInfo query(StrategyQuery qo) throws Exception {
        qo.setPageSize(8);
        return strategyService.query(qo);
    }


    @GetMapping(value="{regionId}/strategies",produces = "text/html;charset=utf-8")
    public void listByRegionId(StrategyQuery qo, HttpServletResponse response) throws Exception {
        //获取当季推荐攻略
        //状态为推荐
        qo.setState(Strategy.STATE_COMMEND);
        //不分页
        qo.setPageSize(0);
        PageInfo commends = strategyService.query(qo);
        //获取全部攻略
        qo.setState(null);
        //要分页
        qo.setPageSize(8);
        PageInfo all = strategyService.query(qo);
        //创建配置对象
        Configuration config = new Configuration(Configuration.VERSION_2_3_23);
        //设置模板目录
        config.setDirectoryForTemplateLoading(new File(servletContext.getRealPath("/template")));
        //获取模板
        Template template = config.getTemplate("strategyTemplate.ftl","utf-8");
        HashMap<Object, Object> data = new HashMap<>();
        data.put("commends",commends);
        data.put("all",all);
        //合成结果
        template.process(data,response.getWriter());
    }


}
