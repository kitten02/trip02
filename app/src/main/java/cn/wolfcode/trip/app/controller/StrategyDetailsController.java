package cn.wolfcode.trip.app.controller;

import cn.wolfcode.trip.base.domain.StrategyDetail;
import cn.wolfcode.trip.base.service.IStrategyDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 攻略文章资源控制器
 */
@RestController
@RequestMapping("strategyDetails")
public class StrategyDetailsController {

   @Autowired
   private IStrategyDetailService detailService;

   @GetMapping("{id}")
   public StrategyDetail getById(@PathVariable Long id){
      return detailService.getById(id);
   }
}
