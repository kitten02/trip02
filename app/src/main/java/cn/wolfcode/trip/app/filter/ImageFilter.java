package cn.wolfcode.trip.app.filter;


import cn.wolfcode.trip.base.util.UploadUtil;
import org.apache.commons.io.FileUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;

public class ImageFilter implements Filter {


    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        //获取当前访问的图片路径
        HttpServletRequest req = (HttpServletRequest) request;
        String uri = req.getRequestURI();
        System.out.println(uri);
        //判断公共区域是否存在该图片
        File file = new File(UploadUtil.PATH, uri);//C://trip/upload/2b0b3cf8-8b56-4e9e-9ab7-27fc94347a6a.jpg
        if (file.exists()){
            //把图片响应给浏览器
            response.getOutputStream().write(FileUtils.readFileToByteArray(file));
        }else{
            //直接放行
            chain.doFilter(request,response);
        }
    }


    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }


    @Override
    public void destroy() {

    }
}
