package cn.wolfcode.trip.app.controller;

import cn.wolfcode.trip.app.util.UserContext;
import cn.wolfcode.trip.base.domain.Strategy;
import cn.wolfcode.trip.base.domain.StrategyCatalog;
import cn.wolfcode.trip.base.domain.StrategyComment;
import cn.wolfcode.trip.base.query.StrategyCommentQuery;
import cn.wolfcode.trip.base.query.StrategyQuery;
import cn.wolfcode.trip.base.service.IStrategyCatalogService;
import cn.wolfcode.trip.base.service.IStrategyCommentService;
import cn.wolfcode.trip.base.service.IStrategyService;
import cn.wolfcode.trip.base.util.JSONResult;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 攻略资源控制器
 */
@RestController
@RequestMapping("strategies")
public class StrategyController {

    @Autowired
    private IStrategyService strategyService;

    @Autowired
    private IStrategyCatalogService catalogService;
    @Autowired
    private IStrategyCommentService commentService;

    @GetMapping
    public PageInfo queryForList(StrategyQuery qo){
      return strategyService.query(qo);
    }

    @GetMapping("{id}")
    public Strategy getById(@PathVariable Long id){
      return strategyService.getById(id);
    }

    @GetMapping("{strategyId}/catalogs")
    public List<StrategyCatalog> listCatalogsById(@PathVariable Long strategyId){
        List<StrategyCatalog> strategyCatalogs = catalogService.listByStrategyId(strategyId);
        return strategyCatalogs;
    }

    @GetMapping("{strategyId}/comments")
    public PageInfo queryCommentsById(StrategyCommentQuery qo){
        qo.setOrderby("sc.createTime desc");
        return commentService.query(qo);
    }

    @PostMapping("{strategy.id}/comments")
    public JSONResult saveComment(StrategyComment comment,String[] tags){
        //设置评论者
        comment.setUser(UserContext.getCurrentUser());
        commentService.saveOrUpdate(comment,tags);
        return new JSONResult();
    }



}
