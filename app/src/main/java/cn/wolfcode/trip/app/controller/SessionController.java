package cn.wolfcode.trip.app.controller;

import cn.wolfcode.trip.app.util.UserContext;
import cn.wolfcode.trip.base.domain.User;
import cn.wolfcode.trip.base.service.IUserService;
import cn.wolfcode.trip.base.util.JSONResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;

/**
 * 会话资源控制器
 */
@RestController
@RequestMapping("sessions")
public class SessionController {

    @Autowired
    private IUserService userService;

    @PostMapping
    public JSONResult login(@RequestParam String email, @RequestParam String password){
        JSONResult jsonResult = new JSONResult();
        try {
           User user = userService.login(email,password);
           //把用户返回给移动端
           jsonResult.setResult(user);
           //把用户放到session中
           UserContext.setCurrentUser(user);
        } catch (Exception e) {
            e.printStackTrace();
            jsonResult.mark(e.getMessage());
        }
        return jsonResult;
    }

    @DeleteMapping
    public JSONResult logout(HttpSession session){
        //注销会话
        session.invalidate();
        return new JSONResult();
    }
}
