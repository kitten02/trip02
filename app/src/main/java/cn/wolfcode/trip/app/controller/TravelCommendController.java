package cn.wolfcode.trip.app.controller;

import cn.wolfcode.trip.base.query.TravelCommendQuery;
import cn.wolfcode.trip.base.service.ITravelCommendService;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 游记推荐资源控制器
 */
@RestController
@RequestMapping("travelCommends")
public class TravelCommendController {

    @Autowired
    private ITravelCommendService commendService;

    @GetMapping
    public PageInfo query(TravelCommendQuery qo){
       return commendService.queryForAppList(qo);
    }
}
