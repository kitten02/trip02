package cn.wolfcode.trip.app.controller;

import cn.wolfcode.trip.base.util.UploadUtil;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.Map;

/**
 * 图片资源控制器
 */
@RestController
@RequestMapping("images")
@Api(value = "图片资源",description = "图片资源控制器")
public class ImageController {

    @PostMapping
    public Map upload(MultipartFile file){

        HashMap<Object, Object> map = new HashMap<>();
        try {
            //String url = UploadUtil.upload(file, UploadUtil.PATH + "/upload");
            //上传图片到七牛云
            String url = UploadUtil.uploadQiu(file);
            map.put("status",1);
            map.put("url",UploadUtil.NIU_PATH+url);//http://phywhaqv1.bkt.clouddn.com/FlSg3E5g6zxGvuplGdkHeIKhQkxt
        } catch (Exception e) {
            e.printStackTrace();
            map.put("status",0);
            map.put("msg",e.getMessage());
        }
        return map;
    }

}
