package cn.wolfcode.trip.app.util;

import cn.wolfcode.trip.base.domain.User;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpSession;

//用户上下文工具类类
public abstract class UserContext {

    private static final String USER_IN_SESSION = "USER_IN_SESSION";

    public static HttpSession getSession() {
        return ((ServletRequestAttributes) RequestContextHolder
                .getRequestAttributes()).getRequest().getSession();
    }

    public static void setCurrentUser(User user) {
        getSession().setAttribute(USER_IN_SESSION, user);
    }

    public static User getCurrentUser() {
        return (User) getSession().getAttribute(USER_IN_SESSION);
    }

}
