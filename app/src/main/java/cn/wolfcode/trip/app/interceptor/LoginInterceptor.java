package cn.wolfcode.trip.app.interceptor;

import cn.wolfcode.trip.app.util.UserContext;
import cn.wolfcode.trip.base.domain.User;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LoginInterceptor extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        //从会话中获取当前登录用户
        User currentUser = UserContext.getCurrentUser();
        if (currentUser!=null){
            return true;
        }

        //跳转到登录页面
        response.sendRedirect("/login.html");

        //不放行
        return false;
    }
}
