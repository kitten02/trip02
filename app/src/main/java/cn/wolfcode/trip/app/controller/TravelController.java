package cn.wolfcode.trip.app.controller;

import cn.wolfcode.trip.app.util.UserContext;
import cn.wolfcode.trip.base.domain.Travel;
import cn.wolfcode.trip.base.query.TravelQuery;
import cn.wolfcode.trip.base.service.ITravelService;
import cn.wolfcode.trip.base.util.JSONResult;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 游记资源控制器
 */
@RestController
@RequestMapping("travels")
public class TravelController {

    @Autowired
    private ITravelService travelService;

    @PostMapping
    public JSONResult save(Travel travel){
        //设置作者
      travel.setAuthor(UserContext.getCurrentUser());
      travelService.saveOrUpdate(travel);
      return new JSONResult();
    }

    @PutMapping("{id}")
    public JSONResult update(Travel travel){
      travelService.saveOrUpdate(travel);
      return new JSONResult();
    }

    @GetMapping("{id}")
    public Travel getById(@PathVariable Long id){
      return travelService.getById(id);
    }


    @GetMapping
    public PageInfo queryForList(TravelQuery qo){
      //发布状态
      qo.setState(Travel.STATE_RELEASE);
      //发布时间倒序
      qo.setOrderby("t.releaseTime desc");
      return travelService.queryForList(qo);
    }
}
