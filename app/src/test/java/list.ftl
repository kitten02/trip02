<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>骡窝窝系统管理平台</title>
    <#include "../common/header.ftl"/>
    <link rel="stylesheet" href="/js/plugins/treeview/dist/bootstrap-treeview.css" type="text/css"/>
    <script src="/js/plugins/treeview/dist/bootstrap-treeview.js"></script>
    <script type="text/javascript">
        $(function () {
            var pid;
            var pname;
            $.get("/region/selectByParentId.do", {type: "tree"}, function (data) {
                $('#treeview1').treeview({
                    data: [{text: "全部地区", nodes: data}],
                    showTags: true,
                    lazyLoad: function (node) {
                        $.get("/region/selectByParentId.do", {parentId: node.id, type: "tree"}, function (data) {
                            $("#treeview1").treeview("addNode", [data, node]);
                        });
                    },
                    //选中某个节点,将查询结果显示到右侧的表格中
                    onNodeSelected: function (event, node) {
                        $.get("/region/selectByParentId.do", {parentId: node.id}, function (data) {
                            pid = node.id;
                            pname = node.text;
                            var temp = "";
                            $.each(data, function (index, ele) {
                                // console.log(ele);
                                temp += "<tr><td>" + (index + 1) + "</td><td>" + ele.name +
                                        "</td><td><a role=\"button\" class=\"edit_Btn \" data-rid='" + ele.id + "' data-rname='" + ele.name + "'>修改</a></td><td>" +
                                        "<a role=\"button\" class=\"stateBtn\" data-rid='" + ele.id + "' data-rstate='" + ele.state + "'>" +
                                        "" + ele.stateName + "</a></td></tr>";
                            })
                            $("tbody").html(temp);
                            //给编辑按钮绑定点击事件,弹出模态框
                            $(".edit_Btn").click(function () {
                                $("#editForm input").val("");
                                $("#editForm input[name='id']").val($(this).data("rid"));
                                $("#editForm input[name='name']").val($(this).data("rname"));
                                $("#editForm input[name='parent.id']").val(pid);
                                $("#parentName").val(pname);
                                $("#parentName").attr("readonly", true);
                                $("#regionModal").modal("show");
                                //提交表单,完成保存操作
                                $("#editForm").ajaxForm(function () {
                                    doSuccess();
                                });
                                $(".regionSubmitBtn").click(function () {
                                    $("#editForm").submit();
                                });
                            });
                            //给修改状态的按钮绑定点击事件,改变状态
                            $(".stateBtn").click(function () {
                                var rid = $(this).data("rid");
                                var rstate = $(this).data("rstate");
                                $.get("/region/changeState.do", {state: rstate, regionId: rid}, function () {
                                    $.messager.confirm("提示", "保存成功", function () {
                                        window.location.reload();
                                    });
                                });
                            });
                        });
                    }
                });
            });
            //给编辑按钮绑定点击事件,弹出模态框
            $(".inputBtn").click(function () {
                $("#editForm input").val("");
                $("#editForm input[name='parent.id']").val(pid);
                $("#parentName").val(pname);
                $("#parentName").attr("readonly", true);
                $("#regionModal").modal("show");
                //提交表单,完成保存操作
                $("#editForm").ajaxForm(function () {
                    doSuccess();
                });
                $(".regionSubmitBtn").click(function () {
                    $("#editForm").submit();
                });
            });
        })
    </script>
</head>

<body>
<div class="container">
<#include "../common/top.ftl"/>
    <div class="row">
        <div class="col-sm-3">
		<#assign currentMenu = "region" />
				<#include "../common/menu.ftl" />
        </div>
        <div class="col-sm-9">
            <div class="page-header">
                <h3>旅游地区管理</h3>
            </div>
            <a role="button" class="btn btn-success inputBtn">
                <span class="glyphicon glyphicon-plus"></span> 添加地区
            </a>
            <div class="row">
                <div class="col-sm-4">
                    <div id="treeview1"></div>
                </div>
                <div class="col-sm-8">
                    <table class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>序号</th>
                            <th>名称</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- 修改地区的模态框 -->
<div class="modal fade" id="regionModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">地区编辑/增加</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="/region/saveOrUpdate.do" method="post" id="editForm">
                    <input type="hidden" name="id" value="">
                    <input type="hidden" name="parent.id" value="">
                    <div class="form-group">
                        <label class="col-sm-4 control-label">地区名称：</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="name" value=""
                                   placeholder="请输入地区名称">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">上级地区：</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="parentName" value="">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-primary regionSubmitBtn">保存</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal -->
</div>
</body>
</html>